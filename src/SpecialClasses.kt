import java.util.*

data class User(val name: String, val surname: String)

fun main() {
    val user = User("Alex", "Xander")
    println(user)

    val secondUser = User("Alex", "Xander")
    val thirdUser = User("Max", "Donald")



    println("user == secondUser: ${user == secondUser}")
    println("user == thirdUser: ${user == thirdUser}")
    println(user.hashCode())
    println(thirdUser.hashCode())

    // Enum
    val state = State.IDLE
    val message = when (state) {
        State.IDLE -> "IDLE"
        State.RUNNING -> "RUNNING"
        State.FINISHED -> "FINISHED"
    }
    println(message)

    // Object Keyword
    val d1 = LuckDispatcher()
    val d2 = LuckDispatcher()
    d1.getNumber()
    d2.getNumber()
}
// Enum
enum class State {
    IDLE, RUNNING, FINISHED
}

// Object Keyword
class LuckDispatcher {
    fun getNumber() {
        var objRandom = Random()
        println(objRandom.nextInt(90))
    }
}