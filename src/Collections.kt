fun main() {
    // map
    val numbers = listOf(1, 5, 3, -4, 7, -11)
    val doubled = numbers.map { x -> x * 2 }
    val tripled = numbers.map { it * 3 }

    println("Numbers: $numbers")
    println("Doubled Numbers: $doubled")
    println("Tripled Numbers: $tripled")

    // sorted
    val shuffled = listOf(5, 4, 2, 1, 3)
    val natural = shuffled.sorted()
    val inverted = shuffled.sortedBy { -it }

    println("Shuffled: $shuffled")
    println("Natural order: $natural")
    println("Inverted natural order: $inverted")

    // any, all, none
    val numbers0 = listOf(1, 2, 3, 4, 5, 55)
    val anyNegative = numbers0.any { it < 0 }
    val anyGT6 = numbers0.any { it > 6 }

    println("Numbers: $numbers0")
    println("Is there any number less than 0: $anyNegative")
    println("Is there any number greater than 6: $anyGT6")

    // count
    val numbers1 = listOf(1, -2, 3, -4, 5, -6 , 7, 8, 9)
    val totalCount = numbers1.count()
    val evenCount = numbers1.count { it % 2 == 0 }

    println("Total number of elements: $totalCount")
    println("Number of even elements: $evenCount")

    //min, max
    val numbers2 = listOf(1, 2, 3)
    val empty = emptyList<Int>()

    println("Numbers: $numbers2, min = ${numbers2.min()} max = ${numbers2.max()}")
    println("Empty: $empty, min = ${empty.min()}, max = ${empty.max()}")
}
