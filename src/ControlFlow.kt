fun main() {
    // When
    cases("Hello")
    cases(1)
    cases(0L)
    cases(MyClass())
    cases("hello")

    // Loop
    val num = listOf("1", "2", "3")
    for (cake in num) {
        println("Yummy, it's a $cake cake!")
    }

    // Ranges
    for(i in 0..5) {
        print(i)
        print(" ")
    }
    println()

    for (i in 5 downTo 0) {
        print(i)
        print(" ")
    }
    println()

    for (c in 'a'..'d') {
        print(c)
    }
    println()

    // Equality Checks
    val authors = setOf("Shakespeare", "Hemingway", "Twain")
    val writers = setOf("Twain", "Shakespeare", "Hemingway")
    println(authors == writers)

    // Conditional Expression
    fun max(a: Int, b: Int) = if (a > b) a else b
    println(max(99, -42))
}
// When
fun cases(obj: Any) {
    when (obj) {
        1 -> println("One")
        "Hello" -> println("Greeting")
        is Long -> println("Long")
        !is String -> println("Not a string")
        else -> println("Unknown")
    }
}
class MyClass
